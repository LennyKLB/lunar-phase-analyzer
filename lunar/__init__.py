import pylunar

GPS_GRONINGEN_LAT = 53
GPS_GRONINGEN_LON = 6


def get_moon_info(dt):
    """Function that retrieves lunar phase information based on date and location
    Constants:
    GPS_GRONINGEN_LAT
    GPS_GRONINGEN_LON -- GPS coordinates for Groningen
    Arguments:
    dt -- Python datetime.datetime object

    Returns:
    pylunar.MoonInfo object with informative methods describing lunar phase
    """
    moon_info = pylunar.MoonInfo(
        (GPS_GRONINGEN_LAT, 0, 0), (GPS_GRONINGEN_LON, 0, 0))

    date_info = (
        dt.year,
        dt.month,
        dt.day,
        dt.hour,
        dt.minute,
        dt.second
    )

    moon_info.update(date_info)

    return {
        'phase': moon_info.phase_name(),
        'fraction': moon_info.fractional_phase(),
        'age': moon_info.age()
    }
