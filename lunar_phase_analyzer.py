from flask import Flask
from flask_restful import Resource, Api, request
from datetime import datetime
import dateutil.parser as dp

from lunar import get_moon_info


DATE_FORMATS = ['YYYY-MM-DD', 'YYYY-MM-DDTHH:MM:SS']
CLIENT_ERROR_MESSAGE = 'Date format error: Dates must be formatted like \
{}'.format(', '.join(DATE_FORMATS))

app = Flask(__name__)
api = Api(app)


class LunarPhase(Resource):
    """Main request handler class for the Lunar Phase calculator"""

    def get(self):
        """GET request handler

        Returns response with either lunar phase info or error info 
        if input was incorrect
        """
        try:
            date = request.args.get('date')
            if date:
                date_time = dp.parse(date)
            else:
                date_time = datetime.now()

            moon_info = get_moon_info(date_time)

            message = {
                'status': 'success',
                'date': date_time.isoformat(' '),
                'moon': moon_info
            }

            return message

        except ValueError as error:
            return {'status': 'failed',
                    'message': CLIENT_ERROR_MESSAGE}, 400

        else:
            return {'status': 'failed', 'message': error}, 500


api.add_resource(LunarPhase, '/', endpoint='lunar')

if __name__ == '__main__':
    app.run(port=3000, host='0.0.0.0')
