# Lunar phase analyzer
REST API to query the phase of the moon based on your current time!

## Requirements
Python 3, virtualenv

## Installation
Set up your virtual environment and install dependencies:
```sh
python3 -m virtualenv lunar-env
source lunar-env/bin/activate
pip install -r requirements.txt
```
## Running
```sh
# source lunar-env/bin/activate
python3 lunar_phase_analyzer.py
```
```* Serving Flask app "lunar_phase_analyzer" (lazy loading)
* Environment: production
WARNING: Do not use the development server in a production environment.
Use a production WSGI server instead.
* Debug mode: off
* Running on http://0.0.0.0:3000/ (Press CTRL+C to quit)
```

## Examples
You can specify the granularity of the date stamp, just make sure it's [valid ISO 8601](https://en.wikipedia.org/wiki/ISO_8601).
```sh
curl http://0.0.0.0:3000?date=2018-11-25T10:00
{"status": "success", "phase": "WANING_GIBBOUS", "fraction": 0.9381668131435992, "age": 17.74857592186163}
```
```sh
curl http://0.0.0.0:3000?date=2018-12-05
{"status": "success", "phase": "WANING_CRESCENT", "fraction": 0.05760640109507881, "age": 27.33190925535746}
```

Omitting the timestamp will force the API to use the current timestamp:
```sh
curl http://0.0.0.0:3000
{"status": "success", "date": "2018-11-25 17:31:27.349816", "moon": {"phase": "WANING_GIBBOUS", "fraction": 0.9195588087724823, "age": 18.062082866301353}}
```
